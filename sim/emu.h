#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
typedef uint64_t UINT64;
typedef uint32_t UINT32;
typedef uint16_t UINT16;
typedef uint8_t UINT8;
typedef int64_t INT64;
typedef int32_t INT32;
typedef int16_t INT16;
typedef int8_t INT8;
typedef long offs_t;
#define INLINE static inline
#define LSB_FIRST
#define TRUE 1
#define FALSE 0
#define ARRAY_LENGTH(x)		(sizeof(x) / sizeof(x[0]))
#define I64FMT "ll"
typedef union PAIR
{
#ifdef LSB_FIRST
	struct { UINT8 l,h,h2,h3; } b;
	struct { UINT16 l,h; } w;
	struct { INT8 l,h,h2,h3; } sb;
	struct { INT16 l,h; } sw;
#else
	struct { UINT8 h3,h2,h,l; } b;
	struct { INT8 h3,h2,h,l; } sb;
	struct { UINT16 h,l; } w;
	struct { INT16 h,l; } sw;
#endif
	UINT32 d;
	INT32 sd;
} PAIR;

struct address_space;
struct ymm_mac_st_s;

typedef void device_t;
typedef struct address_space {
	UINT32 tag;
	UINT32 *mem;
	struct ymm_mac_st_s *machine_state;
	device_t *device;
	void *cpustate;
	/*UINT8 (*read_byte)(struct address_space *as, UINT16 a);*/
	/*void (*write_byte)(struct address_space *as, UINT16 a, UINT8 v);*/
} address_space;
typedef device_t legacy_cpu_device;
typedef int (*device_irq_callback)(device_t *device, int irqnum);
#define ATTR_UNUSED                             __attribute__((__unused__))
#define READ32_HANDLER(name)                    UINT32 name(ATTR_UNUSED address_space *space, ATTR_UNUSED offs_t offset, ATTR_UNUSED UINT32 mem_mask)
#define WRITE32_HANDLER(name)                   void   name(ATTR_UNUSED address_space *space, ATTR_UNUSED offs_t offset, ATTR_UNUSED UINT32 data, ATTR_UNUSED UINT32 mem_mask)
// I/O line definitions
enum
{
        // input lines
        MAX_INPUT_LINES = 32+3,
        INPUT_LINE_IRQ0 = 0,
        INPUT_LINE_IRQ1 = 1,
        INPUT_LINE_IRQ2 = 2,
        INPUT_LINE_IRQ3 = 3,
        INPUT_LINE_IRQ4 = 4,
        INPUT_LINE_IRQ5 = 5,
        INPUT_LINE_IRQ6 = 6,
        INPUT_LINE_IRQ7 = 7,
        INPUT_LINE_IRQ8 = 8,
        INPUT_LINE_IRQ9 = 9,
        INPUT_LINE_NMI = MAX_INPUT_LINES - 3,

        // special input lines that are implemented in the core
        INPUT_LINE_RESET = MAX_INPUT_LINES - 2,
        INPUT_LINE_HALT = MAX_INPUT_LINES - 1
};
// I/O line states
enum line_state
{
	CLEAR_LINE = 0,				// clear (a fired or held) line
	ASSERT_LINE,				// assert an interrupt immediately
	HOLD_LINE,					// hold interrupt line until acknowledged
	PULSE_LINE					// pulse interrupt line instantaneously (only for NMI, RESET)
};

extern double dtime(void);
extern UINT32 ymm_read_dword(address_space *a, offs_t o);
extern void ymm_write_dword(address_space *a,offs_t o,UINT32 d);
extern UINT16 ymm_read16(address_space *a, offs_t o);
extern void ymm_write16(address_space *a,offs_t o,UINT16 d);
extern UINT8 ymm_read8(address_space *a, offs_t o);
extern void ymm_write8(address_space *a,offs_t o,UINT8 d);
#define CPU_RESET_NAME(name)                     cpu_reset_##name
#define CPU_RESET(name)                                  void CPU_RESET_NAME(name)(legacy_cpu_device *device)
#define CPU_EXECUTE_NAME(name)                   cpu_execute_##name
#define CPU_EXECUTE(name)                                void CPU_EXECUTE_NAME(name)(legacy_cpu_device *device)
#define CPU_INIT_NAME(name)                              cpu_init_##name
#define CPU_INIT(name)                                   void CPU_INIT_NAME(name)(legacy_cpu_device *device, device_irq_callback irqcallback)
#define CPU_RESET_CALL(name)                     CPU_RESET_NAME(name)(device)
