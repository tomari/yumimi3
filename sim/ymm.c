// yumimi-sh2 (Yumimi-3) simulator
// Written by Hisanobu Tomari. Public Domain (for parts that I wrote).
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <errno.h>
#include <pthread.h>
#include <math.h>
#include "emu.h"
#include "sh2.h"
#include "sh2comn.h"

/* Address map configuration */
static const UINT16 LM_BYTES=0x800;
static const UINT16 RM_BYTES=0x100;
static const UINT16 RM_IN1_BASE=0x400;
static const UINT16 RM_IN2_BASE=0x600;
static const UINT16 RM_P1_BASE=0x4000;
static const UINT16 RM_P2_BASE=0x6000;
static const UINT16 CONF_BASE=0x8000;
static const UINT16 DIE_ADDR=0x8666;
/* emulation config */
static const UINT32 CLK_GRANULARITY=7;

/* Yumimi machine state */
struct pe_s {
	UINT32 peid;
	sh2_state cpustate;
	address_space as;
};
typedef struct pe_s pe;
enum ymm_type {
	YMM_MLSX,
	YMM_TORUS};
struct ymm_mac_st_s {
	UINT32 nrank;
	UINT32 npipe;
	enum ymm_type type;
	pe *pe_st;
	UINT32 *lm;
	UINT64 total_cycles;
	UINT8 exit_req;
};
typedef struct ymm_mac_st_s ymm_mac_st;

/* function prototypes */
/* static UINT8 pe_read_byte(address_space *as, UINT16 a);
   static void pe_write_byte(address_space *as, UINT16 a, UINT8 v); */

static int init_as(address_space *as, UINT32 peid, UINT8 *rom, struct ymm_mac_st_s *machine_state);
static int init_pe(ymm_mac_st *ms,pe *p, UINT32 peid, UINT8 *rom);
static void run(const char *filename,UINT32 rank, UINT32 pipe,signed long long clks,UINT32 np);
static UINT32* mem_of_pe(ymm_mac_st *ms, UINT32 peid);
static void cleanup(ymm_mac_st *ms);
static UINT32 next_id(ymm_mac_st *ms,UINT32 myid, UINT32 port);
static UINT32 pipe_of_peid(ymm_mac_st *ms, UINT32 peid);
static UINT32 rank_of_peid(ymm_mac_st *ms, UINT32 peid);
static UINT32 peid_of(ymm_mac_st *ms, UINT32 rank, UINT32 pipe);
static int init_ymm(ymm_mac_st *ms,UINT32 rank,UINT32 pipe,enum ymm_type type,const char* romfile);
/*static void run_cycles(ymm_mac_st *ms, UINT32 cycles);*/

/* variables */

/* implementation */
static int init_as(address_space *as, UINT32 peid, UINT8 *rom, struct ymm_mac_st_s *machine_state) {
	int result=0;
	as->tag=peid;
	as->machine_state=machine_state;
	as->mem=&machine_state->lm[(((intptr_t)LM_BYTES)*((intptr_t)peid))/4];
	memcpy(as->mem,rom,LM_BYTES);
	/*as->read_byte=&pe_read_byte;
	  as->write_byte=&pe_write_byte;*/
	return result;
}
static int init_pe(ymm_mac_st *ms,pe *p, UINT32 peid, UINT8 *rom) {
	int result=0;
	cpu_init_sh2(&(p->cpustate),NULL);
	p->cpustate.program=&p->as;
	p->cpustate.internal=&p->as;
	p->peid=peid;
	if(init_as(&p->as,peid,rom,(void*)ms)) {
		result=-1;
	} else {
		p->cpustate.irq_callback=NULL;
		p->cpustate.icount=0;
		cpu_reset_sh2(&(p->cpustate));
	}
	return result;
}

static UINT32 next_id(ymm_mac_st *ms, UINT32 myid, UINT32 port) {
	UINT32 nrank=ms->nrank, npipe=ms->npipe;
	enum ymm_type t=ms->type;
	UINT32 nextid;
	UINT32 nextpipe, nextrank;
	UINT32 pipemask=npipe-1;
	switch(t) {
	case YMM_MLSX:
		nextpipe=((pipe_of_peid(ms,myid)>>1)|(port<<(__builtin_popcount(pipemask)-1)))&pipemask;
		nextrank=rank_of_peid(ms,myid)+1;
		nextid=peid_of(ms,nextrank,nextpipe);
		break;
	case YMM_TORUS:
		nextpipe=pipe_of_peid(ms,myid)+(port?0:1);
		nextrank=rank_of_peid(ms,myid)+(port?1:0);
		nextid=peid_of(ms,nextrank,nextpipe);
		break;
	default:
		nextid=(myid+1)%(nrank*npipe);
		break;
	}
	/*fprintf(stderr,"next(%d)=%d\n",myid,nextid);*/
	return nextid;
}
/* peid to pipe/rank */
/* divide peid to RRRRPPPP */
static UINT32 peid_of(ymm_mac_st *ms, UINT32 rank, UINT32 pipe) {
	UINT32 pipemask=ms->npipe-1;
	UINT32 sla=__builtin_popcount(pipemask);
	return (rank<<sla)|pipe;
}
static UINT32 pipe_of_peid(ymm_mac_st *ms, UINT32 peid) {
	/* pipe count is power of two */
	UINT32 mask,result;
	mask=ms->npipe-1;
	result=peid&mask;
	/*fprintf(stderr,"pipe_of_peid(%08X)=%08X\n",peid,result);*/
	return result;
}
static UINT32 rank_of_peid(ymm_mac_st *ms, UINT32 peid) {
	int i=0;
	UINT32 result;
	while((ms->npipe-1)&(1<<i)) i++; /* i: width of peid */
	result=peid>>i;
	/*fprintf(stderr,"rank_of_peid(%08X)=%08X\n",peid,result);*/
	return result;
}
/* accessor */
extern UINT32 ymm_read_dword(address_space *as, offs_t a) {
	UINT32 result=(UINT32)-1;
	if(a<LM_BYTES) {
		UINT32 *this_mem=as->mem;
		result=htonl(this_mem[a/4]);
	} else if(RM_P1_BASE<=a && a<(RM_P1_BASE+RM_BYTES)) {
		UINT32 dstpeid=next_id(as->machine_state,as->tag,0);
		UINT16 my_base=(pipe_of_peid(as->machine_state,as->tag)&1)?RM_IN2_BASE:RM_IN1_BASE;
		UINT16 offs=a-RM_P1_BASE+my_base;
		UINT32 *dstmem=mem_of_pe(as->machine_state,dstpeid);
		if(dstpeid<(as->machine_state->npipe*as->machine_state->nrank))
			result=htonl(dstmem[offs/4]);
		else
			result=(UINT32)-1;
	} else if(RM_P2_BASE<=a && a<(RM_P2_BASE+RM_BYTES)) {
		UINT32 dstpeid=next_id(as->machine_state,as->tag,1);
		UINT16 my_base=(pipe_of_peid(as->machine_state,as->tag)&1)?RM_IN2_BASE:RM_IN1_BASE;
		UINT16 offs=a-RM_P2_BASE+my_base;
		UINT32 *dstmem=mem_of_pe(as->machine_state,dstpeid);
		if(dstpeid<(as->machine_state->npipe*as->machine_state->nrank))
			result=htonl(dstmem[offs/4]);
		else
			result=(UINT32)-1;
	} else if(CONF_BASE<=a) {
		UINT32 offs=a-CONF_BASE;
		if(offs<0x10) {
			/* offset   0    1    2    3    4    5    6    7    */
			/*          PIDll -LH  -HL  -HH RIDll -LH  -HL  -HH */
			/* offset   8    9    a    b    c    d    e    f    */
			/*          P#ll  -lh  -hl  -hh R#ll  -lh  -hl  -hh */
			UINT16 id_offs=offs;
			UINT32 val;
			switch(id_offs&0x0c) {
			case 0:
				val=pipe_of_peid(as->machine_state,as->tag);
				break;
			case 0x4:
				val=rank_of_peid(as->machine_state,as->tag);
				break;
			case 0x8:
				val=as->machine_state->npipe;
				break;
			default:
				val=as->machine_state->nrank;
				break;
			}
			result=val;
			/*fprintf(stderr,"P%08X type=%02X val=%08X, R[%04X] = %02X\n",
				as->tag,id_offs&0x0c,val,
				a,result);*/
		} else if(offs==0x10) {
			result=__builtin_popcount((as->machine_state->npipe)-1);
		} else {
			fprintf(stderr,"Out-of-range read on P%08X R[%04X] = %02X\n",
				as->tag,(unsigned)a,result);
		}
	}
	{
		UINT32 r,p;
		r=rank_of_peid(as->machine_state,as->tag);
		p=pipe_of_peid(as->machine_state,as->tag);
		if(a&3) 
			fprintf(stdout,"[UNALIGNEDR] %016lld R: %05d P: %05d [%04X]\n",
				(unsigned long long)as->machine_state->total_cycles,
				r,p,(unsigned)a);
		else if(0) {
			fprintf(stdout,"[TARGET] %016lld R: %05d P: %05d [%08X] => %08X\n",
				(unsigned long long)as->machine_state->total_cycles,
				r,p,(unsigned)a,result);
		}
	}

	return result;
}
extern void ymm_write_dword(address_space *as,offs_t a,UINT32 v) {
	if(a&3) {
		UINT32 r,p;
		r=rank_of_peid(as->machine_state,as->tag);
		p=pipe_of_peid(as->machine_state,as->tag);
		/*if(!p) {*/
			fprintf(stdout,"[UNALIGNEDW] %016lld R: %05d P: %05d [%04X] <= %08X\n",
				(unsigned long long)as->machine_state->total_cycles,
				r,p,(unsigned)a,v);
	} else if(0) {
		/*fprintf(stderr,"dword access to %08X\n",(unsigned int)a);*/
		UINT32 r,p;
		r=rank_of_peid(as->machine_state,as->tag);
		p=pipe_of_peid(as->machine_state,as->tag);
		if(!p) {
			fprintf(stdout,"[W] %016lld R: %05d P: %05d [%04X] <= %08X\n",
				(unsigned long long)as->machine_state->total_cycles,
				r,p,(unsigned)a,v);}
	}

	if(a<LM_BYTES) {
		UINT32 *this_mem=as->mem;
		this_mem[a/4]=ntohl(v);
	} else if(RM_P1_BASE<=a && a<(RM_P1_BASE+RM_BYTES)) {
		UINT32 dstpeid=next_id(as->machine_state,as->tag,0);
		UINT16 my_base=(pipe_of_peid(as->machine_state,as->tag)&1)?RM_IN2_BASE:RM_IN1_BASE;
		UINT16 offs=a-RM_P1_BASE+my_base;
		UINT32 *dstmem=mem_of_pe(as->machine_state,dstpeid);
		if(dstpeid<(as->machine_state->npipe*as->machine_state->nrank))
			dstmem[offs/4]=ntohl(v);
	} else if(RM_P2_BASE<=a && a<(RM_P2_BASE+RM_BYTES)) {
		UINT32 dstpeid=next_id(as->machine_state,as->tag,1);
		UINT16 my_base=(pipe_of_peid(as->machine_state,as->tag)&1)?RM_IN2_BASE:RM_IN1_BASE;
		UINT16 offs=a-RM_P2_BASE+my_base;
		UINT32 *dstmem=mem_of_pe(as->machine_state,dstpeid);
		if(dstpeid<(as->machine_state->npipe*as->machine_state->nrank))
			dstmem[offs/4]=ntohl(v);
	} else if(CONF_BASE<=a) {
		UINT32 r,p;
		r=rank_of_peid(as->machine_state,as->tag);
		p=pipe_of_peid(as->machine_state,as->tag);
		/*if(!p) {*/
			fprintf(stdout,"%016lld R: %05d P: %05d [%04X] <= %08X\n",
				(unsigned long long)as->machine_state->total_cycles,
				r,p,(unsigned)a,v);
			/*}*/
		if(a==0x8660) {
			as->machine_state->exit_req=1;
		}
	}
}
extern UINT16 ymm_read16(address_space *a, offs_t o) {
	UINT32 bf;
	UINT16 res;
	if(o&1) {
		fprintf(stderr,"unaligned word read at %08X\n",(unsigned)o);
	}
	bf=ymm_read_dword(a,o&(~(UINT32)2));
	res=(UINT16)(0xffff&(bf>>((o&2)?0:16))); /* bigend */
	return res;
}
extern void ymm_write16(address_space *a,offs_t o,UINT16 d) {
	UINT32 bf;
	if(o&1) {
		fprintf(stderr,"unaligned word write at %08X\n",(unsigned)o);
	}
	bf=ymm_read_dword(a,o&(~(UINT32)3));
	bf&=((0x0ffff)<<((o&2)?16:0));
	bf|=(((UINT32) d)<<((o&2)?0:16));
	ymm_write_dword(a,o&(~(UINT32)3),bf);
}
extern UINT8 ymm_read8(address_space *a, offs_t o) {
	UINT32 bf;
	bf=ymm_read_dword(a,o&(~(UINT32)3));
	return (UINT8)(0xff&(bf>>(8*(3-(o&3)))));
}
extern void ymm_write8(address_space *a,offs_t o,UINT8 d) {
	UINT32 bf;
	bf=ymm_read_dword(a,o&~(UINT32)3);
	bf&=~(0xff<<(8*(3-(o&3))));
	bf|=(((UINT32) d)<<(8*(3-(o&3))));
	ymm_write_dword(a,o&~(UINT32)3,bf);
}

static UINT32* mem_of_pe(ymm_mac_st *ms, UINT32 peid) {
	return ms->pe_st[peid].as.mem;}
static int init_ymm(ymm_mac_st *ms,UINT32 rank,UINT32 pipe,enum ymm_type type,const char *romfile) {
	pe *pe_mem;
	/*UINT8 *lm;*/
	UINT8 *addr;
	int i,j;
	int fd;
	static const int prot=PROT_READ;
	static const int flags=MAP_PRIVATE;
	ms->nrank=rank;
	ms->npipe=pipe;
	ms->type=type;
	ms->exit_req=0;
	if((fd=open(romfile,O_RDONLY))<0) {
		fputs("ROM file open failed.\n",stderr);
		return -2;
	}
	if((addr=mmap(NULL,LM_BYTES,prot,flags,fd,0))==MAP_FAILED) {
		fputs("ROM map failed.\n",stderr);
		fputs(strerror(errno),stderr);
		return -5;
	}
	close(fd);
	if(!(pe_mem=calloc(rank*pipe,sizeof(pe)))) {
		fprintf(stderr,"pe state memory alloc. fail!\n");
		close(fd);
		return -1;
	}
	if(!(ms->lm=calloc(rank*pipe,LM_BYTES))) {	
		fprintf(stderr,"lm alloc. fail!\n");
		close(fd);
		return -3;
	}
	ms->pe_st=pe_mem;
	for(i=0; i<rank; i++) {
		for(j=0; j<pipe; j++) {
			UINT32 peid=i*pipe+j;
			int r=init_pe(ms,&pe_mem[peid],peid,addr);
			if(r<0) {
				fprintf(stderr,"init_pe failed at %d\n",peid);
			}
		}
	}
	munmap(addr,LM_BYTES);
	ms->total_cycles=0;
	return 0;
}

struct worker_arg {
	ymm_mac_st *ms;
	signed long long cycles;
	UINT32 thread_id;
	UINT32 np;
	pthread_barrier_t *barrier;
};

void *ymm_worker(void *warg_) {
	struct worker_arg *warg=(struct worker_arg*)warg_;
	signed long long clks=warg->cycles;
	UINT32 base, num;
	num=(UINT32) ceil(((double)warg->ms->npipe)/((double)warg->np));
	base=warg->thread_id*num;
	while(!warg->ms->exit_req) {
		int rc;
		// exec
		if(base<warg->ms->npipe) {
			UINT32 p;
			for(p=base; p<base+num && p<warg->ms->npipe ; p++) {
				UINT32 r=warg->ms->nrank-1;
				do {
					unsigned long long peid=p*warg->ms->nrank+r;
					warg->ms->pe_st[peid].cpustate.icount+=CLK_GRANULARITY;
					cpu_execute_sh2(&(warg->ms->pe_st[peid].cpustate));
				} while(r--);
			}
		}
		rc=pthread_barrier_wait(warg->barrier);
		if(rc && rc!=PTHREAD_BARRIER_SERIAL_THREAD) {
			fprintf(stderr,"[%d]pthread_barrier_wait failed!\n",warg->thread_id);
		}
		if(clks>0) {
			if(clks>0) {
				clks-=CLK_GRANULARITY;
				if(clks<=0) {
					warg->ms->exit_req=1;
				}
			}
		}
		if(!(warg->thread_id)) {
			warg->ms->total_cycles+=CLK_GRANULARITY;
		}
		rc=pthread_barrier_wait(warg->barrier);
		if(rc && rc!=PTHREAD_BARRIER_SERIAL_THREAD) {
			fprintf(stderr,"[%d]pthread_barrier_wait failed!\n",warg->thread_id);
		}
	}

	return NULL;
}

static void run(const char *filename,UINT32 rank, UINT32 pipe,signed long long clks,UINT32 np) {
	ymm_mac_st ms;
	double start,end;
	if(init_ymm(&ms,rank,pipe,YMM_MLSX,filename)) {
		fprintf(stderr,"init fail\n");
	} else {
		unsigned i;
		pthread_barrier_t bar;
		pthread_t threads[np];
		struct worker_arg targ[np];
		
		if(pthread_barrier_init(&bar,NULL,np)) {
			fprintf(stderr,"pthread_barrier_create failed\n");
		}
		/* init thread args */
		for(i=0; i<np; i++) {
			targ[i].ms=&ms;
			targ[i].cycles=clks;
			targ[i].thread_id=i;
			targ[i].np=np;
			targ[i].barrier=&bar;
		}
		start=dtime();
		for(i=0; i<np; i++) {
			int rc=pthread_create(&threads[i],NULL,ymm_worker,(void*)&targ[i]);
			if(rc) {
				fprintf(stderr,"pthread_create failed, tid=%d, rc=%d\n",i,rc);
			}
		}
		for(i=0; i<np; i++) {
			void *retval;
			int rc=pthread_join(threads[i],&retval);
			if(rc) {
				fprintf(stderr,"pthread_join failed, tid=%d, rc=%d\n",i,rc);
			}
		}
		if(pthread_barrier_destroy(&bar)) {
			fprintf(stderr,"pthread_barrier_destroy failed\n");
		}
		end=dtime();
		cleanup(&ms);
		fprintf(stderr,"%016lld cycles, %f sec\n",(unsigned long long)ms.total_cycles,end-start);
	}
}

static void cleanup(ymm_mac_st *ms) {
	free(ms->lm);
	free(ms->pe_st);
}

extern int main(int argc, char *argv[]) {
	UINT32 cyc,rnk,pip,np;
	if(argc<6) {
		fprintf(stderr,"<ROM> <RANK> <PIPE> <CLK> <NTHREADS>\n");
		exit(EXIT_FAILURE);
	}
	rnk=strtol(argv[2],NULL,0x10);
	pip=strtol(argv[3],NULL,0x10);
	cyc=strtol(argv[4],NULL,0x10);
	np=strtol(argv[5],NULL,10);
	fprintf(stderr,"YMM-sh2 emulator "
		"with Pthreads "
		"#r %05d #p %05d\n",rnk,pip);
	run(argv[1],rnk,pip,cyc,np);
	exit(EXIT_SUCCESS);
	return EXIT_FAILURE;
}
