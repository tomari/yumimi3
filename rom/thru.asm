	.text
vecs:
	.long 0x08 		! initial PC
	.long 0x300             ! initial R15
_start:
	mov #0,r0
	mov.l @(6,pc),r1
	bra loadid
	nop
	.long 0x8000
loadid:
	mov.l @(r0,r1),r14	! r14 <- my pipe id
	add #4,r1
	mov.l @(r0,r1),r13	! r13 <- my rank id
	add #4,r1
	mov.l @(r0,r1),r12	! r12 <- pipe count
	add #4,r1
	mov.l @(r0,r1),r11	! r11 <- rank count
	cmp/eq r0,r13
	bt midrank
	nop
	mov.w @(6,pc),r10	! r10 <- rm_in1
	bra loadrcv0
	nop
	.word 0x300
loadrcv0:
	mov.w @(6,pc),r9	! r9 <- rm_in2
	bra ri2clrd
	nop
	.word 0x380
ri2clrd:
	! wait for tokens
	mov.w @(r0,r10),r8
	cmp/eq r0,r8
	bt ri2clrd
	nop
wait2:
	mov.w @(r0,r9),r8
	cmp/eq r0,r8
	bt wait2
	nop
	add #-1,r11
	cmp/eq r11,r13
	bt lastrnk
midrank:
	mov #1,r1
	mov.w @(6,pc),r8	! r8 <- rm_p1_base
	bra p1ld
	mov.w r1,@(r0,r8)
	.word 0x4000
p1ld:
	mov.w @(6,pc),r8
	bra p2ld
	mov.w r1,@(r0,r8)
	.word 0x6000
p2ld:
	bsr dbgprt
	mov #0x13,r3
	bra fin
	nop
lastrnk:
	bsr dbgprt
	mov #0x14,r3
fin:
	bra fin
	nop
	.align 4
dbgprt: ! debug outputs r3
	mov #0,r0
	mov.l @(6,pc),r2
	bra dbgL1
	nop
	.long 0x8000
dbgL1:
	rts
	mov.l r3,@(r0,r2)

