	.text
vecs:
	.long 0x08 		! initial PC
	.long 0x300             ! initial R15
_start:
	mov #0,r0
	mov.l @(6,pc),r1
	bra loadid
	nop
	.long 0x8000
loadid:
	mov.l @(r0,r1),r14	! r14 <- my pipe id
	add #4,r1
	mov.l @(r0,r1),r13	! r13 <- my rank id
	add #4,r1
	mov.l @(r0,r1),r12	! r12 <- pipe count
	add #4,r1
	mov.l @(r0,r1),r11	! r11 <- rank count
	cmp/eq r0,r13
	bt rank0
	nop
	add #-1,r11
	cmp/eq r11,r13
	bt lastrnk
	nop
	mov #0x13,r3
	bsr dbgprt
	nop
	bra fin
	nop
lastrnk:
	mov #0x14,r3
	bsr dbgprt
	nop
	bra fin
	nop
rank0:
	mov #0x12,r3
	bsr dbgprt
	nop
fin:
	bra fin
	nop
	.align 4
dbgprt: ! debug outputs r3
	mov #0,r0
	mov.l @(6,pc),r2
	bra dbgL1
	nop
	.long 0x8000
dbgL1:
	mov.l r3,@(r0,r2)
	rts
	nop
