	.text
vecs:
	.long 0x08 		! initial PC
	.long 0x400             ! initial R15
_start:
	mov #0,r0
	mov.l @(6,pc),r1
	bra loadid
	nop
	.long 0x8000
loadid:
	mov.l @(r0,r1),r14	! r14 <- my pipe id
	add #4,r1
	mov.l @(r0,r1),r13	! r13 <- my rank id
	add #4,r1
	mov.l @(r0,r1),r12	! r12 <- pipe count
	add #-1,r12
	add #4,r1
	mov.l @(r0,r1),r11	! r11 <- rank count
	add #-1,r11
	cmp/eq r0,r13
	bt rank0
	cmp/eq r11,r13
	bt lastrnk
midrank:			! this PE is intermediate rank or last rank
	bsr open_op		! breaks r10 and r9
	nop
	!debug  !!!!!!!!!!!!!!!!!!!
	bsr dbgprt
	mov #0x62,r3
	!debug	!!!!!!!!!!!!!!!!!!!
	mov.l ymm_inp0,r3
	mov #0,r8
	bsr handle_in
	nop
	mov.l ymm_inp1,r3
	bsr handle_in
	nop
	!debug  !!!!!!!!!!!!!!!!!!!
	bsr dbgprt
	mov #0x64,r3
	!debug	!!!!!!!!!!!!!!!!!!!
	! calc start
	cmp/pl r8
	bf close_op
	bsr calc
	nop
	!debug  !!!!!!!!!!!!!!!!!!!
	bsr dbgprt
	mov #0x65,r3
	!debug	!!!!!!!!!!!!!!!!!!!
close_op:
	mov #1,r10
	mov.l ymm_outp0,r9
	mov.w r10,@r9
	mov.l ymm_outp1,r9
	mov.w r10,@r9
	!debug  !!!!!!!!!!!!!!!!!!!
	bsr dbgprt
	mov #0x66,r3
	!debug	!!!!!!!!!!!!!!!!!!!
	bra midrank
	nop
lastrnk:			! this is the last rank
	mov.l ymm_inp0,r3
	mov.w @r3,r10
	cmp/eq r0,r10
	bt lastrnk
lr_p1:
	mov.l ymm_inp1,r3
	mov.w @r3,r10
	cmp/eq r0,r10
	bt lr_p1
	!debug
	bsr dbgprt
	mov #0x99,r3
	mov.l exit_pt,r3
	mov #1,r1
	mov.l r1,@r3
	!debug
fin:
	bra fin
	nop
	.align 4
exit_pt:
	.long 0x8660
rank0:				! this is teh first rank
	bsr open_op
	nop
	bsr calc
	nop
	mov #1,r10
	mov.l ymm_outp0,r9
	mov.w r10,@r9
	mov.l ymm_outp1,r9
	mov.w r10,@r9	
	nop
	bra fin
	nop
	.align 4
ymm_inp0:
	.long 0x400
ymm_inp1:
	.long 0x600
dbgprt: ! debug outputs r3	
	mov.l dbgaddr,r2
	rts
	mov.l r3,@r2
	.align 4
dbgaddr:
	.long 0x8000
open_op:			! open output ports
	sts.l pr,@-r15
	mov.l ymm_outp0,r10
	bsr oopsr
	nop
	lds.l @r15+,pr
	mov.l ymm_outp1,r10
oopsr:
	mov.w @r10,r9
	cmp/pl r9
	bt oopsr
	add #2,r10
	mov.w r0,@r10
	rts
	nop
	.align 4
ymm_outp0:
	.long 0x4000
ymm_outp1:
	.long 0x6000
handle_in:			! r3: port addr. r8: rcvd packets
	!! INPUT BUFFER  [SYNC.W][#PKT.W]...
	!!               0           2              4      6    8    10   12
	!! PKT STRUCTURE [Distance.W][Destination.W][ori.w][0.w][1.w][2.w][3.w]
	sts.l pr,@-r15
	mov #0,r0
hinl1:
	mov.w @r3,r10
	cmp/eq r0,r10
	bt hinl1
	add #2,r0
	mov.w @(r0,r3),r10	! r10: #packets
	add #2,r0		! r0+r3: head of packets
hil0:
	cmp/pl r10
	bf hiend
	mov.w @(r0,r3),r5	! r5: distance.w
	!debug  !!!!!!!!!!!!!!!!!!!
	!mov.l r3,@-r15
	!bsr dbgprt
	!mov r3,r3
	!mov.l @r15+,r3
	!debug	!!!!!!!!!!!!!!!!!!!	
	cmp/pl r5
	bf handle_here
	! forward r0 to +14 !
	! route
	mov.l skbaddr,r7
	add #-1,r5		! distance--
	mov.w r5,@r7
	!add #2,r0
	!add #2,r7		! r7 dst addr
	mov #6,r6		! loop count
cp2skb5:
	add #2,r0		! r0+r3 src addr
	add #2,r7		! r7 dst addr
	mov.w @(r0,r3),r4
	mov.w r4,@r7
	add #-1,r6
	cmp/pl r6
	bt cp2skb5
	add #2,r0
	add #-10,r7
	mov.w @r7,r9
	! r0: FWD 2+2+2*5
	! r9: destination
	! r5: distance
	bsr routeskb
	nop
hil0e:
	add #-1,r10
	bra hil0
	nop
hiend:
	mov #0,r0
	mov.w r0,@r3	! sync bit set to 0
	lds.l @r15+,pr
	rts
	nop
	.align 4
skbaddr:
	.long skb
handle_here:
	add #1,r8
	add #4,r0
	mov.w @(r0,r3),r9	! r9: ori.w
	shll r9
	shll r9
	shll r9			! r9<= r9*8
	mov.l calbufaddr,r7
	add #2,r0		! r0: head of numbers
	add r7,r9		! r9: edi
	mov #4,r4
hhcpy:
	mov.w @(r0,r3),r7
	mov.w r7,@r9
	add #2,r9
	add #2,r0
	add #-1,r4
	cmp/pl r4
	bt hhcpy
	bra hil0e
	nop
routeskb:			! copy skb contents to send output port
	! r9: destination, r5: distance r7,r6,r4
	sts.l pr,@-r15
	mov #1,r7
rsl0:
	cmp/pl r5
	bf sftend
	shll r7
	bra rsl0
	add #-1,r5
sftend:
	and r7,r9
	cmp/pl r9
	bt rslp1
	bra portok
	mov.l p0addrw,r5
rslp1:
	mov.l p1addrw,r5
portok:
	add #2,r5
	mov.w @r5,r7	! packet count
	mov r7,r6
	add #1,r6
	mov.w r6,@r5
	add #2,r5
	! dst= r5 + 4 + 14*r7
	mov #14,r6
	mulu.w r6,r7
	sts macl,r7
	add r7,r5		! r5: dst for copy
	mov.l r0,@-r15
	mov #0,r0
	mov.l skbaddr3,r4
rscpy: ! r5=dst src=r4(skb)
	mov.w @(r0,r4),r7	
	mov.w r7,@(r0,r5)
	add #2,r0
	cmp/eq #14,r0
	bf rscpy
	mov.l @r15+,r0
	lds.l @r15+,pr
	rts
	nop
	.align 4
calbufaddr:
	.long calbuf
p0addrw:
	.long 0x4000
p1addrw:
	.long 0x6000
skbaddr2:
	.long skb
calc:	! calc r10,r9,r7,r6,r5,r4,r3
	sts.l pr,@-r15
	mov.l skbaddr3,r10
	add #6,r10
	mov.l calccbaddr,r9
	mov.l r0,@-r15
	mov #0,r0
calc_l:
	mov.w @(r0,r9),r6
	mov r9,r7
	add #8,r7
	mov.w @(r0,r7),r5
	add #8,r7
	mov.w @(r0,r7),r4
	shll r6
	mov r5,r3
	add r4,r3
	sub r6,r3
	mov.w r3,@r10
	add #2,r0
	add #2,r10
	cmp/eq #8,r0
	bf calc_l
	mov.l skbaddr3,r10
	mov.l @r15+,r0
	mov.l distreg,r5
	mov.l @r5,r5			! r5: distance
	add #-1,r5
	mov.w r5,@r10		! distance
	add #2,r10
	mov.w r14,@r10		! destination
	mov #0,r7
	add #2,r10
	mov.w r7,@r10		! type
	mov r14,r9
	bsr routeskb		! r9: destination, r5: distance r7,r6,r4
	nop
	mov.l skbaddr3,r10
	mov.w @r10,r5		! distance
	add #2,r10
	mov r14,r9
	add #1,r9
	and r12,r9
	mov.w r9,@r10		! dest
	add #2,r10
	mov #1,r7
	mov.w r7,@r10		! type
	bsr routeskb
	nop
	mov.l skbaddr3,r10
	mov.w @r10,r5
	add #2,r10
	mov r14,r9
	add #-1,r9
	and r12,r9
	mov.w r9,@r10		! dest
	add #2,r10
	mov #2,r7
	mov.w r7,@r10		! type
	bsr routeskb
	nop
	lds.l @r15+,pr
	rts
	nop
	.align 4
skbaddr3:
	.long skb
calccbaddr:
	.long calbuf
distreg:
	.long 0x8010
skb:
	.word 0 ! distance
	.word 0 ! destination
	.word 0 ! type
	.word 0
	.word 0
	.word 0
	.word 0
calbuf:		! dummy data
	.word 283		! 0
	.word 144
	.word 73
	.word 31
	.word 299		! -1
	.word 301
	.word 70
	.word 70
	.word 145		! +1
	.word 322
	.word 111
	.word 521
